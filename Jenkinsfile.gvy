node {
    payload = getPayload()
}

pipeline {
    agent any
    triggers {
        GenericTrigger(
            genericVariables: [
                [key: 'body', value: '$'],
                [expressionType: 'JSONPath', key: 'repositoryName', value: '$.repository.name']
            ],
            causeString: '',
            token: '9eb0a5ef590e53f22261e47fcf9f75cf7e08d033',
            printContributedVariables: false,
            printPostContent: false,
            silentResponse: false,
            regexpFilterText: '$repositoryName',
            regexpFilterExpression: '^(pet-project-service)$'
        )
    }

    environment {
        terraformDevPath = '610716223528/eu-central-1/dev/ecs'
        terraformConifgFile = 'terraform.tfvars'
        slackChannel = '#growerhub_alerts'
        masterBranch = 'origin/master'
        branchName = "${payload.pullrequest.source.branch.name}"
        nickname = "${payload.actor.nickname}"
        pullrequestState = "${payload.pullrequest.state}"
        nodeVersion = 'NodeJS-8.12.0'
    }

    tools { nodejs nodeVersion }

    stages {
        stage('Init') {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS')
                setCurrentBuildProps()
                sh 'env'
                deleteDir()
                checkoutBranch()
            }
        }

        stage('Install') {
            steps {
                sh 'npm i'
            }
        }
        stage('Eslint/Test') {
            steps {
                sh 'npm run eslint'
                sh 'npm run test'
                cobertura coberturaReportFile: '**/coverage/cobertura-coverage.xml'
            }
        }
        stage('Release/Deploy') {
            when {
                expression {
                    return pullrequestState == 'MERGED';
                }
            }
            steps {
                bumpVersion()
                setCurrentBuildProps()
            }
        }
    }
    post {
        always {
            notifyBitBucket ()
            //notifySlack ()
        }
    }
}

def notifyBitBucket() {
    def statusForBitBucket = (currentBuild.currentResult == 'SUCCESS') ? 'SUCCESSFUL' : 'FAILED'
    bitbucketStatusNotify(buildState: statusForBitBucket)
}


def notifySlack() {
    wrap([$class: 'BuildUser']) {
        def buildStatus = currentBuild.currentResult
        def msg = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n${env.BUILD_URL}, triggered by *${nickname}*"
        msg = ('SUCCESS' == buildStatus) ? msg : msg + "\nAttention: @here, please fix the failed build"
        def color = ('SUCCESS' == buildStatus) ? '#00FF00' : '#FF0000'
        slackSend channel: slackChannel, attachments: [[text: msg, color: color]]
    }
}

def bumpVersion() {
    nodejs(nodeJSInstallationName: nodeVersion, configId: 'npm-private-config') {
        sh 'npm version patch -m "NPM RELEASE: v%s"'
        //sh 'npm publish'
        sh 'git push origin HEAD:master --tags'
    }
}


def checkoutBranch() {
    if(env.pullrequestState == 'MERGED'){
        checkout([$class: 'GitSCM', branches: [[name: masterBranch]], userRemoteConfigs: [[url: env.GIT_URL]] ])
    }else{
        checkout([$class: 'GitSCM', branches: [[name: env.branchName]], userRemoteConfigs: [[url: env.GIT_URL]] ])
    }
}

def getPayload(){
    def requestBody = "${env.body}"
    def payload = readJSON text: requestBody
    return payload
}

def getPackageJsonContent() {
    def packageJson = readJSON file: "${env.WORKSPACE}/package.json"
    return packageJson
}

def setCurrentBuildProps(){
    def packageJson = getPackageJsonContent()
    env.serviceVersion = packageJson.version

    if(env.pullrequestState == 'MERGED'){
        currentBuild.displayName = 'Master v: ' + env.serviceVersion
        currentBuild.description = 'Author: '  + nickname
    }else{
        currentBuild.displayName = 'PR: ' + payload.pullrequest.title
        currentBuild.description = 'Author: '  + nickname
    }
}
