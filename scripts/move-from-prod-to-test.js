const { elasticClient } = require('growerhub-service-lib');
const config = require('config');

const HOSTS = {
  PROD: 'https://search-growerhub-es-temp-prod-qa7kx34nnyexquciveh5zqv5ze.eu-central-1.es.amazonaws.com/',
  TEST: 'https://search-growerhub-es-test-j6rtdlhqg6pg56xnapxqto244i.eu-central-1.es.amazonaws.com/',
  DEV: 'https://search-growerhub-es-dev-sshaaserhar6ok6mshg2zdei4i.eu-central-1.es.amazonaws.com/',
};

const CLIENTS = {
  PROD: elasticClient(HOSTS.PROD, config.get('logLevel'), config.get('elastic.region')),
  TEST: elasticClient(HOSTS.TEST, config.get('logLevel'), config.get('elastic.region')),
  DEV: elasticClient(HOSTS.DEV, config.get('logLevel'), config.get('elastic.region')),
};

const log4js = require('log4js');
log4js.configure(require('../config/log4js'));
const logger = log4js.getLogger('import-restore-index');

const { index, type } = config.get('elastic');

(async () => {
  await copyPlots('PROD', 'TEST', '56SV8EVtPowr5bnyE50pB4');
})();

async function copyPlots(source, target, farmId) {
  const sourceClient = CLIENTS[source];
  const targetClient = CLIENTS[target];
  const filters = `farm:https://data.syngenta.org/growerhub/farms/${farmId}`;
  try {
    const res = await sourceClient.search(index, '', 1000, 1, filters, 'and');
    logger.info(res);

    const plots = res['hits']['hits'].map((plot) => {
      return plot['_source'];
    });

    const updatedPlots = await targetClient.bulkCreate(index, type, 'guid', plots);

    logger.info(updatedPlots);

  } catch (e) {
    logger.error(e);
  }

}

// async function restoreIndex(index, schema){
//   const exist = await checkIndices(index).catch(err => logger.error(err));
//   if (exist) {
//     logger.info(index, 'index already exists');
//     return Promise.resolve();
//   }
//   const res = await createIndices(index, schema).catch(err => logger.error(err));
//   return Promise.resolve(res);
// }
//
//
// async function checkIndices(index) {
//   return new Promise((resolve, reject) => {
//     client.indices.exists({ index }, (err, res) => {
//       if (err) {
//         return reject(err);
//       }
//       return resolve(res);
//     });
//   });
// }
//
//
// async function createIndices(index, schema) {
//   return new Promise((resolve, reject) => {
//     client.indices.create({
//       index,
//       body: schema
//     }, (err, res) => {
//       if(err) return reject(err);
//       logger.info(index, 'index has been successfully created');
//       return resolve(res);
//     });
//   });
// }
