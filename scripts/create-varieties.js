const { elasticClient } = require('growerhub-service-lib');
const config = require('config');

const HOST = 'http://localhost:9200/';

const CLIENT = elasticClient(HOST, config.get('logLevel'), config.get('elastic.region'));

const log4js = require('log4js');
log4js.configure(require('../config/log4js'));
const logger = log4js.getLogger('import-restore-index');

const index = 'plant-varieties';
const type = 'plant-variety';

(async () => {
  await createVarieties();
})();

async function createVarieties() {
  const client = CLIENT;
  const varieties = require('./all-varieties.json');
  try {
    while (varieties.length > 0){
      const batchVarieties = varieties.slice(0,100);
      const updatedPlots = await client.bulkCreate(index, type, 'guid', batchVarieties);
      logger.info(updatedPlots);
    }

  } catch (e) {
    logger.error(e);
  }

}
