const { elasticClient } = require('growerhub-service-lib');
const config = require('config');

const HOSTS = {
  TEST: 'https://search-growerhub-es-test-j6rtdlhqg6pg56xnapxqto244i.eu-central-1.es.amazonaws.com/',
};

const CLIENTS = {
  TEST: elasticClient(HOSTS.TEST, config.get('logLevel'), config.get('elastic.region')),
};

const log4js = require('log4js');
log4js.configure(require('../config/log4js'));
const logger = log4js.getLogger('import-restore-index');

const { index, type } = config.get('elastic');

(async () => {
  await createPlots('TEST');
})();

async function createPlots(environment) {
  const client = CLIENTS[environment];
  const plots = require('./test');
  try {
    
    const updatedPlots = await client.bulkCreate(index, type, 'guid', plots);

    logger.info(updatedPlots);

  } catch (e) {
    logger.error(e);
  }

}
